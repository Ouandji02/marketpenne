export const LIST_ARRIVAL = [
    {
        name: 'bag of mode to hollywood',
        price:'$895.5',
        image:'assets/images/product_10.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'Fujifilm X100T 16 MP Digital Camera (Silver)',
        price:'$253.50',
        image:'assets/images/product_9.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: '',
        price:'$500.00',
        image:'assets/images/product_8.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'bag of mode to hollywood',
        price:'$253.00',
        image:'assets/images/product_7.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'a avengers last saison of beach',
        price:'$250.00',
        image:'assets/images/product_6.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'a game of strong blast',
        price:'$250.00',
        image:'assets/images/product_5.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'Fujifilm X100T 16 MP Digital Camera (Silver)',
        price:'$520.00',
        lastPrice:'$590.00',
        image:'assets/images/product_1.png',
        reduction:20,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'Samsung CF591 Series Curved 27-Inch FHD Monitor',
        price:'$610.00',
        image:'assets/images/product_2.png',
        reduction:false,
        new:true,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'Blue Yeti USB Microphone Blackout Edition',
        price:'$120.00',
        image:'assets/images/product_3.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    {
        name: 'DYMO LabelWriter 450 Turbo Thermal Label Printer',
        price:'$235.00',
        image:'assets/images/product_4.png',
        reduction:false,
        new:false,
        stock:'2',
        nbreDeVente:''
    },
    
]

