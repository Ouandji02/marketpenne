import React from 'react'

export default function Shop() {
  return (
    <div class="banner" id='shop'>
        <div class="container">
          <div class="row">
            <div class="col-md-4">
              <div
                class="banner_item align-items-center"
                style={{ backgroundImage: "url(assets/images/banner_1.jpg)" }}
              >
                <div class="banner_category">
                  <a href="#">women's</a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div
                class="banner_item align-items-center"
                style={{ backgroundImage: "url(assets/images/banner_2.jpg)" }}
              >
                <div class="banner_category">
                  <a href="#">accessories's</a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div
                class="banner_item align-items-center"
                style={{ backgroundImage: "url(assets/images/banner_3.jpg)" }}
              >
                <div class="banner_category">
                  <a href="#">men's</a>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
  )
}
