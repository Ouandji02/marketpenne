import React from "react";
import { LIST_BEST_SELLER } from "../data/ListBestSeller";

export default function Seller() {

  const list = LIST_BEST_SELLER
  return (
    <div class="best_sellers" id="seller">
      <div class="container">
        <div class="row">
          <div class="col text-center">
            <div class="section_title new_arrivals_title">
              <h2>Best Sellers</h2>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col">
            <div
              class="product-grid"
              data-isotope='{ "itemSelector": ".product-item", "layoutMode": "fitRows" }'
            >
              {list.map((item, key) => (
                <div key={key} class="product-item men">
                  <div class="product discount product_filter">
                    <div class="product_image">
                      {item.reduction ? (
                        <>
                          <div class="product_bubble product_bubble_right product_bubble_red d-flex flex-column align-items-center">
                            <span>-20</span>
                          </div>
                        </>
                      ) : item.new ? (
                        <>
                          <div class="product_bubble product_bubble_left product_bubble_green d-flex flex-column align-items-center">
                            <span>new</span>
                          </div>
                        </>
                      ) : null}
                      <img src={item.image} alt="" />
                    </div>
                    <div class="favorite favorite_left"></div>
                    <div class="product_info">
                      {item.reduction ? (
                        <>
                          <div class="product_price">
                            {item.price}
                            <span>$590.00</span>
                          </div>
                        </>
                      ) : item.new ? (
                        <>
                          <div class="product_price">{item.price}</div>
                        </>
                      ) : (
                        <div class="product_price">{item.price}</div>
                      )}
                      <h6 class="product_name">
                        <a href="#">{item.name}</a>
                      </h6>
                    </div>
                  </div>
                  <div class="red_button add_to_cart_button">
                    <a href="#">add to cart</a>
                  </div>
                </div>
              ))}
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
