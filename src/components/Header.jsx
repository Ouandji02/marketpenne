import React from 'react'

export default function Header() {
  return (
    <header class="header trans_300" id='header'>
        <div class="main_nav_container">
          <div class="container">
            <div class="row">
              <div class="col-lg-12 text-right">
                <div class="logo_container">
                  <a href="#">
                    Market<span>Penne</span>
                  </a>
                </div>
                <nav class="navbar">
                  <ul class="navbar_menu">
                    <li>
                      <a href="#home">home</a>
                    </li>
                    <li>
                      <a href="#shop">shop</a>
                    </li>
                    <li>
                      <a href="#arrival">arrival</a>
                    </li>
                    <li>
                      <a href="#seller">best seller</a>
                    </li>
                    <li>
                      <a href="#blog">blog</a>
                    </li>
                    <li>
                      <a href="#newsletter">newsletter</a>
                    </li>
                    <li>
                      <a href="#contact">contact</a>
                    </li>
                  </ul>
                  <ul class="navbar_user">
                    <li>
                      <a href="#">
                        <i class="fa fa-search" aria-hidden="true"></i>
                      </a>
                    </li>
                    <li>
                      <a href="#">
                        <i class="fa fa-user" aria-hidden="true"></i>
                      </a>
                    </li>
                    <li class="checkout">
                      <a href="#">
                        <i class="fa fa-shopping-cart" aria-hidden="true"></i>
                        <span id="checkout_items" class="checkout_items">
                          2
                        </span>
                      </a>
                    </li>
                  </ul>
                  <div class="hamburger_container">
                    <i class="fa fa-bars" aria-hidden="true"></i>
                  </div>
                </nav>
              </div>
            </div>
          </div>
        </div>
      </header>
  )
}
