import React from 'react'

export default function MenuHamburger() {
  return (
    <div class="hamburger_menu">
        <div class="hamburger_close">
          <i class="fa fa-times" aria-hidden="true"></i>
        </div>
        <div class="hamburger_menu_content text-right">
          <ul class="menu_top_nav">
            <li class="menu_item">
              <a href="#home">home</a>
            </li>
            <li class="menu_item">
              <a href="#shop">shop</a>
            </li>
            <li class="menu_item">
              <a href="#arrival">arrival</a>
            </li>
            <li class="menu_item">
              <a href="#seller">best seller</a>
            </li>
            <li class="menu_item">
              <a href="#">blog</a>
            </li>
            <li class="menu_item">
              <a href="#newletter">newletter</a>
            </li>
            <li class="menu_item">
              <a href="#">contact</a>
            </li>
          </ul>
        </div>
      </div>
  )
}
