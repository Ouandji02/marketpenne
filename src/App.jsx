import { useState } from "react";
import Arrival from "./components/Arrival";
import Blogs from "./components/Blogs";
import Footer from "./components/Footer";
import GetStarted from "./components/GetStarted";
import GetStarted2 from "./components/GetStarted2";
import Header from "./components/Header";
import MenuHamburger from "./components/MenuHamburger";
import Newletter from "./components/Newletter";
import Rules from "./components/Rules";
import Seller from "./components/Seller";
import Shop from "./components/Shop";
import logo from "./logo.svg";

function App() {
  return (
    <div className="MainDiv">
      <Header/>
      <MenuHamburger/>
      <GetStarted/>
      <Shop/>
      <Arrival/>
      <GetStarted2/>
      <Seller/>
      <Rules/>
      <Blogs/>
      <Newletter/>
      <Footer/>
    </div>
  );
}

export default App;
